package org.example;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuButton;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.util.Callback;
import org.example.ReadFileTask.OnTodoListener;

public class MainController implements Initializable {

    @FXML
    private MenuButton mbGgSheet;
    @FXML
    private Text txtProgress, txtPgMessage;
    @FXML
    private ProgressBar proBar;
    @FXML
    private HBox container;
    @FXML
    private Button btnAdd, btnDelete, btnLoad;
    @FXML
    private ListView<Todo> lvTodoList;
    @FXML
    private TextField tfSearch;

    private final Model model = new Model();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        lvTodoList.setCellFactory(new Callback<ListView<Todo>, ListCell<Todo>>() {
            @Override
            public ListCell<Todo> call(ListView<Todo> param) {
                return new TodoListCell();
            }
        });

        lvTodoList.setItems(model.todoList().sorted());

        lvTodoList.getSelectionModel().selectedItemProperty().addListener(
            new ChangeListener<Todo>() {
                @Override
                public void changed(ObservableValue<? extends Todo> observable, Todo oldValue,
                    Todo newValue) {
                    if (newValue != null)
                        System.out.println(newValue.getName());
                }
            });

        btnAdd.disableProperty().bind(tfSearch.textProperty().isEmpty());
        btnDelete.disableProperty().bind(lvTodoList.getSelectionModel()
            .selectedItemProperty().isNull());
        proBar.visibleProperty().bind(btnLoad.disabledProperty().or(mbGgSheet.disabledProperty()));
        txtProgress.visibleProperty().bind(proBar.visibleProperty().and(proBar.progressProperty().greaterThan(0)));
        txtProgress.textProperty().bind(proBar.progressProperty()
            .multiply(100)
            .asString("%.2f%%\t")
        );
        txtPgMessage.visibleProperty().bind(proBar.visibleProperty());
    }

    @FXML
    public void add() {
        String name = tfSearch.getText();
        if (!name.isEmpty()) {
            Todo todo = new Todo(name, System.currentTimeMillis());
            model.todoList().add(todo);
            tfSearch.clear();
        }
    }

    @FXML
    public void delete() {
        Todo selectedTodo = lvTodoList.getSelectionModel().getSelectedItem();
        if (selectedTodo != null)
            model.todoList().remove(selectedTodo);
    }

    public void load(ActionEvent actionEvent) {
        btnLoad.setDisable(true);
        ReadFileTask task = new ReadFileTask("/big.txt");
        proBar.progressProperty().bind(task.progressProperty());
        task.setOnNewTodo(new OnTodoListener() {
            @Override
            public void onNew(Todo todo) {
                model.todoList().add(todo);
            }
        });

        task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent event) {
                btnLoad.setDisable(false);
            }
        });
        task.setOnFailed(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent event) {
                task.getException().printStackTrace();
            }
        });
        new Thread(task).start();
    }

    public void importFromGgSheet(ActionEvent actionEvent) {
        mbGgSheet.setDisable(true);
        ReadSheetTask task = new ReadSheetTask();
        proBar.progressProperty().bind(task.progressProperty());
        txtPgMessage.textProperty().bind(task.messageProperty());
        task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent event) {
                List<Todo> todoList = task.getValue();
                System.out.println("Size = " + todoList.size());
                model.todoList().setAll(todoList);
                mbGgSheet.setDisable(false);
            }
        });
        task.setOnFailed(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent event) {
                task.getException().printStackTrace();
                mbGgSheet.setDisable(false);
            }
        });
        new Thread(task).start();
    }

    public void exportToGgSheet(ActionEvent actionEvent) {
    }
}
