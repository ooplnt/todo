package org.example;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import org.apache.commons.text.WordUtils;

public class ReadFileTask extends Task<List<Todo>> {

    private final String path;

    private static final int MAX_LENGTH = 128;

    private OnTodoListener onTodoListener;


    public ReadFileTask(String path) {
        this.path = path;
    }

    public void setOnNewTodo(OnTodoListener onTodoListener) {
        this.onTodoListener = onTodoListener;
    }

    @Override
    protected List<Todo> call() throws Exception {
        List<Todo> list = new ArrayList<>();
        InputStream inputStream = getClass().getResourceAsStream(path);
        Path jPath = Paths.get(getClass().getResource(path).toURI());
        long fileSize = Files.size(jPath);
        int total = (int) Math.ceil(1.0f * fileSize / MAX_LENGTH);
        System.out.println(total);
        int progress = 0;
        Scanner sc = null;
        updateProgress(progress, fileSize);
        try {
            sc = new Scanner(inputStream, "UTF-8");
            sc.useDelimiter("");
            StringBuilder b = new StringBuilder();
            while (true) {
                if (sc.hasNext()) {
                    char c = sc.next().charAt(0);
                    b.append(c);
                    if (b.length() == MAX_LENGTH) {
                        progress++;
                        updateProgress(progress, total);
                        Todo todo = createTodo(b);
                        list.add(todo);
                        Platform.runLater(() -> onTodoListener.onNew(todo));
                        b = new StringBuilder();
                    }
                } else {
                    progress++;
                    updateProgress(progress, total);
                    Todo todo = createTodo(b);
                    list.add(todo);
                    Platform.runLater(() -> onTodoListener.onNew(todo));
                    break;
                }
            }
            // note that Scanner suppresses exceptions
            if (sc.ioException() != null) {
                throw sc.ioException();
            }
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
            if (sc != null) {
                sc.close();
            }
        }
        return list;
    }

    private Todo createTodo(StringBuilder b) {
        String name = b.toString().trim().replaceAll("\\R+", " ");
        name = WordUtils.capitalize(name);
        Todo todo = new Todo(name, new Random().nextLong());
        return todo;
    }

    public interface OnTodoListener {
        void onNew(Todo todo);
    }
}
