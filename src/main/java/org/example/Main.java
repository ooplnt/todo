package org.example;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void init() throws Exception {
        super.init();
        System.out.println("Init app");
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        System.out.println("Stop app");
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        System.out.println("Start App");

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/main.fxml"));
        Parent root = loader.load();
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.setMinHeight(400);
        primaryStage.setMinWidth(300);
        primaryStage.show();
    }
}