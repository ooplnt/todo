package org.example;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

public class TodoListCell extends ListCell<Todo> implements Initializable {

    @FXML
    private HBox box;
    @FXML
    private CheckBox cbDone;
    @FXML
    private Text txtTitle;
    @FXML
    private Text txtDeadline;

    public TodoListCell() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/list-cell.fxml"));
            loader.setController(this);
            loader.setRoot(this);
            loader.load();
        } catch (IOException exc) {
            // handle exception
            exc.printStackTrace();
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        txtTitle.wrappingWidthProperty().bind(box.widthProperty().subtract(64));
    }

    @Override
    protected void updateItem(Todo item, boolean empty) {
        super.updateItem(item, empty);
        setText(null);
        if (empty || item == null) {
            setGraphic(null);
        } else {
            txtTitle.setText(item.getName());
            boolean isDone = item.doneProperty().get();
            cbDone.setSelected(isDone);
            txtTitle.setStrikethrough(isDone);
            txtDeadline.setStrikethrough(isDone);
            txtDeadline.setText(DateTimeUtils.toString(item.getDeadline()));
            cbDone.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    getListView().getSelectionModel().select(item);
                    item.doneProperty().set(!isDone);
                    updateItem(item, empty);
                }
            });
            setGraphic(box);
        }
    }
}
