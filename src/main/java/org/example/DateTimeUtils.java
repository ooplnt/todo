package org.example;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTimeUtils {

    private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("dd/MM/yyyy");

    public static String toString(long dateTime) {
        return FORMATTER.format(new Date(dateTime));
    }

    public static long toTimestamp(String dateInString) {
        long deadline;
        try {
            Date date = FORMATTER.parse(dateInString);
            deadline = date.getTime();
        } catch (ParseException e) {
            deadline = 0;
            e.printStackTrace();
        }
        return deadline;
    }
}
