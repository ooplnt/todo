package org.example;

import java.util.ArrayList;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Model {

    private final ObservableList<Todo> todoList = FXCollections.observableList(
        new ArrayList<>(),
        e -> new Observable[]{e.doneProperty()}
    );

    public Model() {
        todoList.add(new Todo("Hello 1", System.currentTimeMillis()));
        todoList.add(new Todo("Hello 2", System.currentTimeMillis()));
    }

    public ObservableList<Todo> todoList() {
        return todoList;
    }
}
