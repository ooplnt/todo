package org.example;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

public class Todo implements Comparable<Todo> {

    private BooleanProperty done = new SimpleBooleanProperty();

    private final String name;

    private final long deadline;

    public Todo(String name, long deadline) {
        this.name = name;
        this.deadline = deadline;
    }

    public BooleanProperty doneProperty() {
        return done;
    }

    public String getName() {
        return name;
    }

    public long getDeadline() {
        return deadline;
    }

    @Override
    public int compareTo(Todo o) {
        int compareStatus = Boolean.compare(done.get(), o.done.get());
        if (compareStatus == 0) {
            return Long.compare(deadline, o.deadline) * -1;
        } else {
            return compareStatus;
        }
    }
}
